from flask import Flask, send_from_directory, Blueprint

from api.bezirke import bezirke
from api.kandidaten import kandidaten
from api.parteien import parteien
from api.wahlen import wahlen
from api.raw import raw
from api.ergebnisse import ergebnisse

app = Flask(__name__)

# serve the dynamic stuff
app.register_blueprint(raw, url_prefix="/api/raw")

app.register_blueprint(bezirke, url_prefix="/api/bezirke")
app.register_blueprint(kandidaten, url_prefix="/api/kandidaten")
app.register_blueprint(parteien, url_prefix="/api/parteien")
app.register_blueprint(wahlen, url_prefix="/api/wahlen")

app.register_blueprint(ergebnisse, url_prefix="/api/ergebnisse")


# serve the statics!!!1!
@app.route('/', defaults={'path': 'index.html'})
@app.route("/<path:path>")
def serve_frontend(path):
    return send_from_directory("frontend", path)
