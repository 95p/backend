from json import dumps, loads

from flask import Blueprint, Response, request

from FileManager import FileManager

kandidaten = Blueprint("kandidaten", __name__)


@kandidaten.route("/", methods=["GET"])
def get_kandidaten():
    state = FileManager.instance.get_state()
    ps = list(filter(lambda key: state[key]["type"] == "kandidat", state))

    parteien_dict = {}
    for key in ps:
        parteien_dict[key] = state[key]
    print(parteien_dict)

    return Response(dumps(parteien_dict, ensure_ascii=False), mimetype="application/json")


@kandidaten.route("/<kandidat>", methods=["GET"])
def get_kandidat(kandidat):
    state = FileManager.instance.get_state()

    ps = filter(lambda key: state[key]["type"] == "kandidat", state)
    print(ps)
    p = next(filter(lambda key: key == kandidat, ps))

    return Response(dumps(state[p], ensure_ascii=False), mimetype="application/json")


@kandidaten.route("/<kandidat>", methods=["PUT"])
def update_kandidat(kandidat):
    json = FileManager.instance.get_json()

    print(request.get_json())
    json.append({
        "UUID": kandidat,
        "changes": loads(request.data)
    })
    FileManager.instance.set_json(json)

    return Response()
