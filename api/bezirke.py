from json import dumps, loads

from flask import Blueprint, Response, request

from FileManager import FileManager

bezirke = Blueprint("bezirke", __name__)


@bezirke.route("/", methods=["GET"])
def get_bezirke():
    state = FileManager.instance.get_state()
    ps = list(filter(lambda key: state[key]["type"] == "bezirk", state))

    parteien_dict = {}
    for key in ps:
        parteien_dict[key] = state[key]
    print(parteien_dict)

    return Response(dumps(parteien_dict, ensure_ascii=False), mimetype="application/json")


@bezirke.route("/<bezirk>", methods=["GET"])
def get_bezirk(bezirk):
    state = FileManager.instance.get_state()

    ps = filter(lambda key: state[key]["type"] == "bezirk", state)
    print(ps)
    p = next(filter(lambda key: key == bezirk, ps))

    return Response(dumps(state[p], ensure_ascii=False), mimetype="application/json")


@bezirke.route("/<bezirk>", methods=["PUT"])
def update_bezirk(bezirk):
    json = FileManager.instance.get_json()

    print(request.get_json())
    json.append({
        "UUID": bezirk,
        "changes": loads(request.data)
    })
    FileManager.instance.set_json(json)

    return Response()
