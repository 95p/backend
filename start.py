from werkzeug.serving import run_simple

from CryptoManager import CryptoManager
from server import app

import sys
from FileManager import FileManager

if __name__ == "__main__":
    FileManager.instance = FileManager(sys.argv[1] + ".json")
    CryptoManager.instance = CryptoManager(sys.argv[1] + ".keys")

    run_simple("0.0.0.0", "8080", application=app)
