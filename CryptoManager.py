import gnupg
from json import loads, dumps


class CryptoManager:
    instance = None

    def __init__(self, keys_file_path):
        self.gpg = gnupg.GPG(gnupghome="/tmp/gnupgtmp")
        with open(keys_file_path) as keys_file:
            self.gpg.import_keys(keys_file.read())

    def validate_json_message(self, json_message):
        signature = json_message["signature"]
        del json_message["signature"]
        json_str = dumps(json_message)
        is_valid = self.gpg.verify(signature + "\n\n" + json_str)

        if is_valid:
            return json_message
        else:
            return False
